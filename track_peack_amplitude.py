import animate_from_list as anim
import numpy as np
import numpy as np
import matplotlib.pyplot as pl
import matplotlib.ticker as tick
import matplotlib
from matplotlib import rcParams

from params import *
import animate_from_list as ani

if __name__ == "__main__":
    # Loading binary data
    data_bin_file = tmp_bin_file + '.npz'
    data = np.load(data_bin_file)

    print "\nVariables stored in file %s:" % data_bin_file
    print data.files

    x = data['x']
    p = data['p']
    u = data['u']

    # Defining data to be plotted
    pp = p - np.mean(p)
    up = u - np.mean(u)

    print "\nPrinting the shape of input data: (%s, %s)" % p.shape

    p_amp = np.amax(np.abs(pp), axis=1)

    print "printing shape of p_amp:"
    print p_amp.shape

    ani.get_tex_layout()

    fig = pl.figure()
    #ax = pl.axes(xlim=(0, 0), ylim=(0, 0))
    ax = pl.axes()

    xlabel='lkhsdfklhds'
    ylabel='lksdjflkndslfd'

    ax.set_xlabel(xlabel)
    ax.set_ylabel(ylabel)
    
    ax.plot(p_amp)
    pl.tight_layout()
    pl.show()

    print "\nDone.\n"
