from antares import *
import numpy as np

from params import *

def fluct(base, zone, inst, var):
    mean = np.mean(base[:][:][var])
    fluctuation = base[zone][inst][var] - mean
    return fluctuation
    

if __name__ == "__main__":
    verbose(2)

    r=Reader('hdf_avbp')
    r['filename']=meshpath
    r['shared'] = True
    base=r.read()

    print base[0]

    # ------------------
    # Reading the files
    # ------------------
    reader = Reader('hdf_avbp')
    reader['base'] = base
    reader['filename'] = solpath
    base = reader.read()

    print base[0][0]

    # ------------------
    # Define line
    # ------------------
    nb = 1001
    p1 = [-1.0, 0., 0.]
    p2 = [ 1.0, 0., 0.]

    # ------------------------
    # Interpolate on the line
    # ------------------------
    line = Treatment('line')
    line['base'] = base
    line['point1'] = p1
    line['point2'] = p2
    line['nbpoints'] = nb
    line['memory_mode'] = True
    new_base = line.execute()

    print 'printing newbase'
    print new_base
    print new_base[0]
    print new_base[0][0]
    print 'Done printing newbase'

    if True:

        x = []
        p = []
        u = []


        for zone in new_base:
            for inst in new_base[zone]:
                # Data for computation
                rhou_b = new_base[zone][inst]['rhou']
                rho_b = new_base[zone][inst]['rho']
                p_b = new_base[zone][inst]['pressure']

                # Data for export
                x.append(new_base[zone][inst]['x'])
                u_b = rhou_b / rho_b

                u.append(u_b)
                p.append(p_b)

                # fluctuations
                p_fluct = p_b - np.mean(p_b)
                u_fluct = u_b - np.mean(u_b)


    out_variables = x, p, u

    if False:
        # ------------------------
        # Write result
        # ------------------------
        w = Writer()
        w['base'] = new_base
        w['filename'] = outputpath + '.dat'
        w['file_format'] = 'column'
        w.dump()

    # Saving data as binary using numpy
    tmp_bin_file='data_bin'
    np.savez(tmp_bin_file, x=x, p=p, u=u)

